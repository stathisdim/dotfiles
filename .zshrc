# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

export TERM="xterm-256color"
# Path to your oh-my-zsh installation.
# export ZSH="/home/stathis/.oh-my-zsh"

#ZSH_THEME="robbyrussell"

#plugins=(git virtualenv virtualenvwrapper python)

#source $ZSH/oh-my-zsh.sh
#source /usr/share/powerlevel9k/powerlevel9k.zsh-theme
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
fpath+=("$HOME/.zsh/pure")
fpath+=("~/.zsh/completion")

autoload -U promptinit; promptinit
prompt pure

# custom aliases
alias c='code .'
alias py='python3'
# startx on boot
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi

export VISUAL=vim
export EDITOR="$VISUAL"

export PATH=~/.npm-global/bin:$PATH
export PATH=/usr/local/go/bin:$PATH

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory
fpath+=${ZDOTDIR:-~}/.zsh_functions
